import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import SearchInput from './SearchInput.js';
import List from './List.js';



class Search extends Component {
constructor(){
  super();
  this.state.textValue;
  this.getTextVal = this.getTextVal.bind(this);

}
  
getTextVal(e){
  this.setState({textValue:e.target.value});
}

  render() {
    return (
      <div>
<SearchInput changeval={this.getTextVal}/>
<List txtval = {this.state.textValue}/>
      </div>
    );
  }
}

export default Search;
