import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class SearchInput extends Component {
  constructor(props){
    super(props);
    this.inputChange = this.inputChange.bind(this);

  }
  inputChange(e){
    this.props.changeval = e.target.value;
  }
  render() {
    return (
      <div className="App">
        <input type="text" name="search" onBlur={this.inputChange}/>
        <button name="search">Search</button>
      </div>
    );
  }
}

export default SearchInput;
